package app;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;

import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.MouseEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

import javax.swing.JOptionPane;

import transforms.Camera;
import transforms.Mat4;
import transforms.Mat4PerspRH;
import transforms.Vec3D;
import utils.MeshGenerator;
import utils.OGLBuffers;
import utils.OGLTexture;
import utils.OGLTexture2D;
import utils.ShaderUtils;
import utils.ToFloatArray;

public class Renderer implements GLEventListener, MouseListener,
		MouseMotionListener, KeyListener, MouseWheelListener {

	int width, height, ox, oy;

	GL2 gl;
	OGLBuffers buffers;
	OGLTexture texture, textureNormal, textureHeight;
	
	int locTime, locMat, locMatPro, observerPosition, lightPosition, spotDirection, funcT, shaderProgram;
	float time = 0;
	
	MeshGenerator meshGenerator;
	
	Camera cam = new Camera();

	float x, y, z;
	Mat4 proj;

	int functionType = 0;
	
	public void init(GLAutoDrawable glDrawable) {
		gl = glDrawable.getGL().getGL2();
		
		System.out.println("Init GL is " + gl.getClass().getName());
		System.out.println("OpenGL version " + gl.glGetString(GL2.GL_VERSION));
		System.out.println("OpenGL vendor " + gl.glGetString(GL2.GL_VENDOR));
		System.out.println("OpenGL renderer " + gl.glGetString(GL2.GL_RENDERER));
		System.out.println("OpenGL extension " + gl.glGetString(GL2.GL_EXTENSIONS));
	
		shaderProgram = ShaderUtils.loadProgram(gl, "/shaders/surface");
			
		createBuffers(gl);

		texture = new OGLTexture2D(gl, "/textures/bricks_d.jpg");
		textureNormal = new OGLTexture2D(gl, "/textures/bricks_n.jpg");
		textureHeight = new OGLTexture2D(gl, "/textures/bricks_h.jpg");
		
		locTime = gl.glGetUniformLocation(shaderProgram, "speed");
		locMat = gl.glGetUniformLocation(shaderProgram, "matMV");
		locMatPro = gl.glGetUniformLocation(shaderProgram, "matP");
		observerPosition = gl.glGetUniformLocation(shaderProgram, "obPos");
		lightPosition = gl.glGetUniformLocation(shaderProgram, "lightPos");
		funcT = gl.glGetUniformLocation(shaderProgram, "typFce");
		spotDirection = gl.glGetUniformLocation(shaderProgram, "spotDirection");

		cam = cam.withPosition(new Vec3D(5, 5, 2.5)).withAzimuth(Math.PI * 1.25).withZenith(Math.PI * -0.125);
		
		gl.glEnable(GL2.GL_DEPTH_TEST);
		
	}
	
	void createBuffers(GL2 gl) {
		// vertices are not shared among triangles (and thus faces) so each face
		// can have a correct normal in all vertices
		// also because of this, the vertices can be directly drawn as GL_QUADS
		// (four vertices form one quad) without any index buffer
		float[] cube = {
				// bottom (z-) face
				0, 0, 0,	0, 0, -1,
				1, 0, 0,	0, 0, -1,
				1, 1, 0,	0, 0, -1,
				0, 1, 0,	0, 0, -1,
				// top (z+) face
				0, 0, 1,	0, 0, 1,
				1, 0, 1,	0, 0, 1,
				1, 1, 1,	0, 0, 1,
				0, 1, 1,	0, 0, 1,
				// x+ face
				1, 0, 0,	1, 0, 0,
				1, 1, 0,	1, 0, 0,
				1, 1, 1,	1, 0, 0,
				1, 0, 1,	1, 0, 0,
				// x- face
				0, 0, 0,	-1, 0, 0,
				0, 1, 0,	-1, 0, 0,
				0, 1, 1,	-1, 0, 0,
				0, 0, 1,	-1, 0, 0,
				// y+ face
				0, 1, 0,	0, 1, 0,
				1, 1, 0,	0, 1, 0,
				1, 1, 1,	0, 1, 0,
				0, 1, 1,	0, 1, 0,
				// y- face
				0, 0, 0,	0, -1, 0,
				1, 0, 0,	0, -1, 0,
				1, 0, 1,	0, -1, 0,
				0, 0, 1,	0, -1, 0
		};
		
		buffers = MeshGenerator.createLineGrid(gl, 300, 300, "inPosition");
	}

	public void display(GLAutoDrawable glDrawable) {
		gl = glDrawable.getGL().getGL2();
		
		gl.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);

		time += 0.1;

		x = -(float) Math.cos(time/10.0) * 6;
		y = (float) Math.sin(time/10.0) * 6;
		z = 2;
		
		gl.glUseProgram(shaderProgram); 
		gl.glUniformMatrix4fv(locMat, 1, false, ToFloatArray.convert(cam.getViewMatrix()), 0);
		gl.glUniformMatrix4fv(locMatPro, 1, false, ToFloatArray.convert(proj), 0);
		
		gl.glUniform1f(locTime, time);
		gl.glUniform1i(funcT, functionType);
		gl.glUniform3f(observerPosition, (float) cam.getPosition().getX(), (float) cam.getPosition().getY(), (float) cam.getPosition().getZ());
		
		gl.glUniform3f(lightPosition, x, y, z);
		gl.glUniform3f(spotDirection, -x, -y, -z);
		
		texture.bind(shaderProgram, "texture", 0);
		textureNormal.bind(shaderProgram, "textureNormal", 1);
		textureHeight.bind(shaderProgram, "textureHeight", 2);
		
		buffers.draw(GL2.GL_TRIANGLES, shaderProgram);
	}

	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {
		this.width = width;
		this.height = height;
		proj = new Mat4PerspRH(Math.PI / 4, height / (double) width, 0.01, 1000.0);
	}

	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged,
			boolean deviceChanged) {
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
		ox = e.getX();
		oy = e.getY();
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseDragged(MouseEvent e) {
		cam = cam.addAzimuth((double) Math.PI * (ox - e.getX()) / width).addZenith((double) Math.PI * (e.getY() - oy) / width);
		ox = e.getX();
		oy = e.getY();
	}

	public void mouseMoved(MouseEvent e) {
	}

	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_W:
			cam = cam.forward(1);
			break;
		case KeyEvent.VK_D:
			cam = cam.right(1);
			break;
		case KeyEvent.VK_S:
			cam = cam.backward(1);
			break;
		case KeyEvent.VK_A:
			cam = cam.left(1);
			break;
		case KeyEvent.VK_SHIFT:
			cam = cam.up(1);
			break;
		case KeyEvent.VK_CONTROL:
			cam = cam.down(1);
			break;
		case KeyEvent.VK_SPACE:
			cam = cam.withFirstPerson(!cam.getFirstPerson());
			break;
		case KeyEvent.VK_H:
			JOptionPane.showMessageDialog(null, "Nápověda \n Kamera: Levé tlačítko myši \n Pohyb: WSAD \n Změna funkce: \n - F1 - vodní hladina \n - F2 - koule \n - F3 - plát \n - F4 - přesýpací hodiny \n - F5 - válec \n - F6 - objekt");
			break;
		case KeyEvent.VK_F1:
			functionType = 1;
			break;
		case KeyEvent.VK_F2:
			functionType = 2;
			break;
		case KeyEvent.VK_F3:
			functionType = 3;
			break;
		case KeyEvent.VK_F4:
			functionType = 4;
			break;
		case KeyEvent.VK_F5:
			functionType = 5;
			break;
		case KeyEvent.VK_F6:
			functionType = 6;
			break;
		}
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		
	}

	public void keyReleased(KeyEvent e) {
	}

	public void keyTyped(KeyEvent e) {
	}

	public void dispose(GLAutoDrawable glDrawable) {
	}
}