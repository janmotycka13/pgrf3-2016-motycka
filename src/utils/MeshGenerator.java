package utils;

import java.util.ArrayList;
import java.util.List;

import com.jogamp.opengl.GL2;

import utils.OGLBuffers;
import utils.ToFloatArray;
import utils.ToIntArray;
import transforms.Vec2D;

public class MeshGenerator {
	
	public static OGLBuffers createLineStrip(final GL2 gl, final int n, final String attribName) { 
		final float[] vertexData = new float[n];
		
		
		for (int i = 0; i < n; i++) {
			vertexData[i] = (float) i / (n - 1);
		}
		
		final OGLBuffers.Attrib[] attributes = { new OGLBuffers.Attrib(attribName, 1) };
		
		
		return new OGLBuffers(gl, vertexData, attributes, null);
	}
	
	public static OGLBuffers createLineGrid(final GL2 gl, final int m, final int n, final String attribName) {
		final List<Vec2D> vertexData = new ArrayList<>();
		for(int i = 0; i < m; i++)
			for (int j = 0; j < n; j++)
				vertexData.add(new Vec2D((double) j / (n-1), (double) i / (m-1)));
		
		final List<Integer> indexData = new ArrayList<>();
		for(int i = 0; i < m - 1; i++)
			for (int j = 0; j < n; j++) {
				indexData.add(i * n + j);
				indexData.add(i * n + j + 1);
				indexData.add((i + 1) * n + j);
				indexData.add(i * n + j + 1);
				indexData.add((i + 1) * n + j);
				indexData.add((i + 1) * n + j + 1);
			}
		
		final OGLBuffers.Attrib[] attributes = { new OGLBuffers.Attrib(attribName, 2) };

		return new OGLBuffers(gl, ToFloatArray.convert(vertexData), attributes, ToIntArray.convert(indexData));
	}
}
