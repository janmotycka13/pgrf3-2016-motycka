#version 150
in vec2 inPosition;
out vec3 vertColor;
out vec2 texCoord;
out vec3 viewDirection;
out vec3 lightDirection;
out vec3 spotLightDirection;
out vec3 normal;
out vec4 position;
out float distance;
uniform float speed;
uniform mat4 matMV;
uniform mat4 matP;
uniform float fce;
uniform vec3 obPos;
uniform vec3 lightPos;
uniform int typFce;

const float PI = 3.1415926535;


vec3 sphere(vec2 paramPos){
	float azimuth = paramPos.x * 2 * PI + speed / 5;
	float zenith = (0.5 - paramPos.y) * PI;
	float r = cos(zenith);
	return vec3(
		r * cos(azimuth),
		r * sin(azimuth),
		sin(zenith)
	);
}
vec3 plane(vec2 paramPos){
	vec3 position;
	float azimuth = sqrt(paramPos.x * paramPos.x + paramPos.y * paramPos.y);
	float zenith  = acos(paramPos.x / azimuth);
	float r  = atan(paramPos.y, paramPos.x);

	return vec3(
		azimuth * sin(zenith) * cos(r) + cos(speed*0.1),
		azimuth * sin(zenith) * sin(r),
		azimuth * cos(r)
	);
}

vec3 sandGlass(vec2 paramPos){
	paramPos.x -= 0.5; paramPos.x *= 2.0;
	paramPos.y = paramPos.y * 2.0 * PI;
	return vec3 (
		paramPos.x * cos(paramPos.y)*sin(paramPos.y)*paramPos.x,
		paramPos.x * sin(paramPos.y)+cos(speed*0.1),
		paramPos.x
	);
}

vec3 object(vec2 paramPos) {

	float r = paramPos.x;
	float theta = paramPos.x;
	float z = paramPos.y;

	return vec3(
		r*cos(theta)+(cos(speed*0.1)),
		r*sin(theta),
		z
	);
}

vec3 waterSurface(vec2 paramPos){
	vec3 position;

	position.xy = paramPos.xy;
	position.xy -= 0.5;
	position.xy *= 10.0;
	
	float dist = 0.05 + sqrt( position.x*position.x + position.y*position.y );
	
	float z = sin( -speed + 2.0 * position.x*position.x + 2.0 * position.y*position.y ) / 5.0;
	
	position.z = z / (dist*2.0);
	
	return position;
}

vec3 roller(vec2 paramPos){
	paramPos.x = paramPos.x - 0.5;
	paramPos.y = paramPos.y - 0.5;
	paramPos.x = paramPos.x * 2 * PI;
	paramPos.y = paramPos.y * 2*PI;
	
	vec3 position;
	position.xy = paramPos.xy;
	
	position.z = -sin(position.x + (speed / 5)); 
	position.x = cos(position.x + (speed / 5));
	return position;
}

vec3 funcType (vec2 paramPos){
	
	switch(typFce){
		default:
		case 1: return waterSurface(paramPos); 
		case 2: return sphere(paramPos);
		case 3: return plane(paramPos);
		case 4: return sandGlass(paramPos);
		case 5: return roller(paramPos);
		case 6: return object(paramPos);
	}
	
}

vec3 normalDerivate (vec2 paramPos){
	float delta = 0.001;
	vec2 dx = vec2(delta, 0), dy = vec2(0, delta);
	vec3 tx = funcType(paramPos + dx) - funcType(paramPos - dx);
	vec3 ty = funcType(paramPos + dy) - funcType(paramPos - dy);
	return normalize(cross(tx, ty));
}

vec3 tangentDerivate(vec2 paramPos) {
	float delta = 0.001;
	vec3 dzdu = (funcType(paramPos + vec2(delta, 0)) - funcType(paramPos - vec2(delta, 0))) / 2.0 / delta;
	return dzdu;
}



void main() {
	
	vec4 position = vec4(funcType(inPosition), 1.0);
	
	vec3 lightPosition;
	position = matMV * position;
	lightPosition = (matMV * vec4(lightPos,1.0)).xyz;
	
	mat3 normalMatrix = inverse(transpose(mat3(matMV)));
	
	viewDirection = obPos - position.xyz;
	lightDirection = lightPosition - position.xyz;
	
    vec3 normal = normalize(normalMatrix * normalDerivate( inPosition ));
    vec3 tangent = normalize(normalMatrix * tangentDerivate( inPosition ));
    vec3 binormal = normalize(cross( normal, tangent ));

    mat3 matTBN = mat3(tangent, binormal, normal);

    viewDirection = normalize( viewDirection * matTBN );
    lightDirection = normalize( lightDirection * matTBN );
  
  	distance = length( lightPos );
  
    gl_Position = matP * position;
  
    texCoord = inPosition;
} 
