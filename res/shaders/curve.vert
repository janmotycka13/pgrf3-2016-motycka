#version 150
in float inParamPos; // input from the vertex buffer
out vec3 vertColor; // output from this shader to the next pipleline stage
uniform mat4 mat; // variable constant for all vertices in a single draw

vec3 curve(float paramPos) {
	return vec3(
		cos(2*3.145927 * paramPos),
		sin(2*3.145927 * paramPos),
		1 + paramPos // 2 - kruh
	);
}

void main() {
	vec3 position = curve(inParamPos);
	gl_Position = mat * vec4(curve(inParamPos), 1.0);
	vertColor = vec3(inParamPos, 1 - inParamPos, position.x);
} 
