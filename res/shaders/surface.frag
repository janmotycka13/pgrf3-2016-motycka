#version 330
in vec3 vertColor;
out vec4 outColor;
in vec3 viewDirection;
in vec3 lightDirection;
in vec3 spotLightDirection;
in vec3 normal;
in vec4 position;
in float distance;
in vec2 texCoord;
uniform sampler2D texture;
uniform int reflektor;
uniform sampler2D textureNormal;
uniform sampler2D textureHeight;
uniform vec3 obPos;


void main() {
	vec2 cBumpSize = vec2(0.04, -0.02);
	float specularPower = 90.0;
	vec3 ambient = vec3(0.2, 0.2, 0.2);
	vec3 diffuse = vec3(0.5, 0.5, 0.5);
	vec3 specular = vec3(0.3, 0.3, 0.3);
	float constantAttenuation = 0.1;
	float linearAttenuation = 0.05; 
	float quadraticAttenuation = 0.005;
	float spotLightConeAngle = 2.5;
	vec2 texCoord2 = texCoord - floor(texCoord);
    float height = texture2D(textureHeight, texCoord2).r;
    height = height * cBumpSize.x + cBumpSize.y;
    vec3 eye = normalize(obPos);
    vec2 offset = eye.xz * height;
    texCoord2 = texCoord2 + normalize(viewDirection).xy * offset;
    vec3 baseColor = texture2D(texture, texCoord2).rgb; 
    vec3 normal = normalize(texture2D(textureNormal, texCoord2).rgb * 2.0 - 1.0);
    vec3 ambientTotal = ambient * baseColor;
    float nDotL = max(dot(normal, lightDirection), 0.0);
    vec3 diffuseTotal = nDotL * diffuse * baseColor;
    vec3 halfVector = normalize(lightDirection + viewDirection);
    float nDotHV = max(dot(normal, halfVector), 0.0);
    vec3 specularTotal = pow(nDotHV, specularPower) * specular;
    float attenuation = 1.0/(constantAttenuation + (linearAttenuation * distance) + quadraticAttenuation * distance * distance);
    
    if(reflektor == 1){
    	vec3 spotLightConeDirection = (-lightDirection);
	    float spotEffect = degrees(acos(dot( (-spotLightDirection), normalize(spotLightConeDirection))));
	    
	    if( spotEffect < spotLightConeAngle ){
	      attenuation = 0.0;
	    }
	    
    }
    
    outColor = vec4((ambientTotal + attenuation * (diffuseTotal + specularTotal)),1.0);
  
}
